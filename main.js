
  var express = require("express");
  var path = require("path");

  var app = express();
  
  var profDetails = [];

  var createProfile = function(profile) {
    return ({
      email:profile.email,
      password:profile.password,
      name:profile.name,
      gender:profile.gender,
      dob:profile.dob,
      address:profile.address,
      country:profile.country,
      contactNo:profile.contactNo
    });
  };

  app.get("/create-profile", function(req, resp) {
      profDetails.push(createProfile(req.query));
      console.log(profDetails);
      resp.status(201).end();
  });

  app.get("/get-profile", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(profDetails);
  });

  app.use(express.static(path.join(__dirname, "/public")));

  app.use("/lib", express.static(path.join(__dirname, "/bower_components")));
    

  app.set("port", parseInt(process.argv[2]) || 3000);


  app.listen(app.get("port"), function() {
    console.log("Application started at %s on port %d", new Date(), app.get("port"));
  });
