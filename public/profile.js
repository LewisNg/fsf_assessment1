(function() {
	var Profile = angular.module("Profile", []);

	var ProfileSvc = function($http) {

		var profileSvc = this;

		profileSvc.createProfile = function(profileDetail) {
			return ($http.get("/create-profile", {
				params: profileDetail
			}));
		};

		profileSvc.getProfile = function() {
			return ($http.get("/get-profile")
				.then(function(result) {
					return (result.data);
				}));
		};        
	};

	//Define a service
	Profile.service("ProfileSvc", [ "$http", ProfileSvc ]);

})();