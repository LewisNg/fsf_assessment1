
(function () {

    var RegApp = angular.module("RegApp", ["Profile"]);
    var createObj = function (ctrl) {
        return ({
            email: ctrl.email,
            password: ctrl.password,
            name: ctrl.name,
            gender: ctrl.gender,
            dob: ctrl.dob,
            address:ctrl.address,
            country:ctrl.country,
            contactNo:ctrl.contactNo
        });
    }

    var RegCtrl = function(ProfileSvc) {
        
        var regCtrl = this; 
        regCtrl.profileArr = [];

        var pwText = "At least 8 characters, contain at least one lowercase character (a-z), one uppercase character (A-Z), one digit (0-9), and one special character";
        regCtrl.pwHelp = pwText;
        var dobText = "Please enter your date of birth";
        regCtrl.dobHelp = dobText;
        var conText = "Please enter your contact number";
        regCtrl.conHelp = conText;
        var emailText = "Please enter your email";
        regCtrl.emailHelp = emailText;

        var pwOK = false;
        var ageOK = false;
        var contactOK = false;
        var emailOK = false;
        
        regCtrl.pwValidate = function() {
            var value = regCtrl.password;            
            if (value == null){
                regCtrl.pwHelp = pwText;
            }else if (value.match(/[a-z]/)
                    && value.match(/[A-Z]/)
                    && value.match(/[1-9]/)
                    && value.match(/[^0-9a-zA-Z]/)
                    && value.length >= 8) {
                pwOK = true;
                regCtrl.pwHelp = "";
            }else if (!value.match(/[a-z]/)){
                regCtrl.pwHelp = "Must contain at least one lowercase character (a-z)";
            }else if (!value.match(/[A-Z]/)){
                regCtrl.pwHelp = "Must contain at least one uppercase character (A-Z)";
            }else if (!value.match(/[1-9]/)){
                regCtrl.pwHelp = "Must contain at least one digit (0-9)";
            }else if (!value.match(/[^0-9a-zA-Z]/)){
                regCtrl.pwHelp = "Must contain at least one special character";
            }else if (value.length < 8){
                regCtrl.pwHelp = "Must be at least 8 characters long";
            }
        };

        regCtrl.ageCheck = function() {
            
            var birth = regCtrl.dob;
            var today = new Date();
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var fBirth = Date.UTC(birth.getFullYear(), birth.getMonth(), birth.getDate());
            var fToday = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate());
            var age = Math.floor((fToday - fBirth) / _MS_PER_DAY)/(12*30);

            console.log(age);
            if (regCtrl.dob.getFullYear() > today.getFullYear()) {
                regCtrl.dobHelp = "Please enter a valid d.o.b";
            }else {
                if (age == null){
                    regCtrl.dobHelp = dobText;
                }else if (age < 18) {
                    regCtrl.dobHelp = "Must be at least 18 years old";
                }else if (age > 200) {
                    regCtrl.dobHelp = "Please enter a valid d.o.b";
                }else if (age >= 18 && age <=200) {
                    ageOK = true;
                    regCtrl.dobHelp = "";
                }
            }
        };
     
        regCtrl.contactCheck = function() {
            var reg = /^[\+|\-]?[(]?[0-9]{1,3}[)]?[-\s\.]?[0-9]{6,20}$/im;
            var con = regCtrl.contactNo;
            
            if (regCtrl.contactNo == null) {
                regCtrl.conHelp = conText;
            }else {
                if (con.match(reg)) {
                    contactOK = true;
                    regCtrl.conHelp = "";
                }else {
                    regCtrl.conHelp = "Invalid contact number";
                }
            }
        };

        regCtrl.emailCheck = function() {
            var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
            var mail = regCtrl.email;

            if (regCtrl.email == null) {
                regCtrl.emailHelp = emailText;
            }else if (mail.match(reEmail)) {
                emailOK = true;
                regCtrl.emailHelp = "";
            }else {
                regCtrl.emailHelp = "Invalid email";
            }
        };

        regCtrl.createProfile = function() {            
        ProfileSvc.getProfile()
        .then(function(profDetails) {
            regCtrl.profileArr = profDetails;
            var idx = regCtrl.profileArr.findIndex(x => x.email==regCtrl.email);
            console.log(idx);

            if (idx == -1 && pwOK && ageOK && contactOK && emailOK) {
                ProfileSvc.createProfile(createObj(regCtrl))
                        .then(function() {
                            console.log("profile created");
                        });
                alert("Registration successful!");
                window.location.href = "/";                  
            }else if (idx != -1) {
                alert("Email has been taken. Please enter another email.")
                regCtrl.email = "";
            };
        }); 

        };
    }
    RegApp.controller("RegCtrl", [ "ProfileSvc", RegCtrl ]);
})();